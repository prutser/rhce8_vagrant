# RHCE8 exercises Vagrant

Vagrant setup for excercises RHCE 8 preparation for EX294
following Cert Guide of Sander van Vugt

No solutions in here. Only day 1 setup

Howto start:
Get Vagrant https://www.vagrantup.com
Install VirtualBox

$ git clone https://gitlab.com/RonaldDenOtter/rhce8_vagrant.git  
$ cd rhce8_vagrant 

$ vagrant up  

After its finished.  

$ vagrant ssh control  
$ sudo su - ansible   
or   
$ sudo su -  

Finished?  
  
vagrant halt  
  
or if start all over with:  
  
vagrant destroy  

#todo
I suspect not everything is covered yet. So additions will be made. Also the vdi files probably have to be changed a little.
